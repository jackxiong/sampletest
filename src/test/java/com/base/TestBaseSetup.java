package com.base;

import java.io.File;
import java.text.DecimalFormat;
import java.util.Random;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Parameters;


public class TestBaseSetup {
	
	 protected static WebDriver browser;
	 
	 static String driverPath = "./drivers/";
	 

	  
	 public TestBaseSetup() {
		 
		  System.out.println("TestBaseSetup");
	  }
	 private static WebDriver initfirefoxDriver(String appURL){
		  
		 System.setProperty("webdriver.gecko.driver", driverPath
					+ "geckodriver");
		  WebDriver driver = new FirefoxDriver();
		  driver.get(appURL);
		  return driver;
	  }
	 private static WebDriver initIEDriver(String appURL){
		  
		  System.setProperty("webdriver.ie.driver", driverPath
					+ "IEDriverServer.exe");
		  WebDriver driver = new InternetExplorerDriver();
		  driver.get(appURL);
		  return driver;
	  }
	  
	 private static WebDriver initchromeDriver(String appURL){
		
		  System.setProperty("webdriver.chrome.driver", driverPath+"chromedriver.exe");
		
		  WebDriver driver = new ChromeDriver();
		  driver.get(appURL);
		  return driver;
	  }
	 
	 private  static WebDriver headlessBrowser (String os,String appURL) {
		    
		    System.out.println("=>"+driverPath+os+"/"+"chromedriver" + " "+appURL);
		    
			System.setProperty("webdriver.chrome.driver",driverPath+os+"/"+"chromedriver"); 
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--no-sandbox");
			options.addArguments("--disable-dev-shm-usage");
			options.addArguments("--headless"); 
			options.addArguments("--headless", "--window-size=1920, 1080"); 
			options.setBinary("/usr/bin/google-chrome");
			WebDriver  driver = new ChromeDriver(options);
			//driver.get(appURL);
			try {
				Thread.sleep(2*1000);
			}catch(Exception error) {
				error.printStackTrace();
			}
			return driver;
			
	 }
	 
	 /*
	   * @author Jack.xiong
	   * 
	   *The parameters need to be defined into testng.xml file based on different browsertype 
	   *to lunch broswer then connect to the appURL.
	   *
	   */
	  
	  @Parameters({"browserType","appURL"})
	  @BeforeClass(alwaysRun=true)
	  
	  public void initializeTestBaseSetup(String browserType,String appURL) {
		  
		  System.out.println("start browser "+browserType);
		  
		  
		  if(browserType.equals("chrome")){
			  
			  browser=  initchromeDriver(appURL);
			  getDriver();
			 
			  
		  }else if (browserType.equals("iexplore")){
			  
			  browser= initIEDriver(appURL);
			  
		  }else if(browserType.equals("firefox")){
			  
			  browser=  initfirefoxDriver(appURL);
			 
		  }else if (browserType.equals("Linux")) {
			  browser= headlessBrowser ("Linux",appURL);
			  getDriver();
		  }
		  
		  else {
			  System.out.println("browser : " + browserType
						+ " is invalid, Launching Firefox as browser of choice..");
		  }
		  
	  }
	  
	  public static void takeSnapShot(WebDriver webdriver,String path) {
			 
			 String n =nums();
			 String fileWithPath = "./screens/"+path+"_"+n+".jpg";
			 
			 try {
				    Thread.sleep(3000);
				 	TakesScreenshot scrShot =((TakesScreenshot)webdriver);
			        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
			        File DestFile=new File(fileWithPath);
			        FileUtils.copyFile(SrcFile, DestFile);
			       
			        System.out.println("captured screen.."+fileWithPath);
			 } catch (Exception error) {
				 System.out.println("failed to capture screens");
				 error.printStackTrace();
			 }
		       

		   }
	  
	  public static String nums() {
			
		    Random rand = new Random();
	        int num1 = (rand.nextInt(7) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
	        int num2 = rand.nextInt(743);
	        int num3 = rand.nextInt(10000);

	        DecimalFormat df3 = new DecimalFormat("000"); 
	        DecimalFormat df4 = new DecimalFormat("0000"); 

	        String phoneNumber = df3.format(num1) + "-" + df3.format(num2) + "-" + df4.format(num3);
	       
	        return phoneNumber;
	}
	  
	  public static WebDriver getDriver()

		{
			return browser;
		}
	 

	  @AfterClass(alwaysRun=true)
	  public void afterClass() {
		  
		  System.out.println("finished testing...");
		  browser.manage().deleteAllCookies();
		  try {
				Thread.sleep(3*1000);
			}catch(Exception error) {
				error.printStackTrace();
			}
		  browser.close();
		  browser.quit();
		  
		  
		
		  
	  }
	  

}
