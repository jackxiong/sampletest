package com.ui.test;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.base.TestBaseSetup;
import com.objects.NabPage;

public class TestScripts extends TestBaseSetup {

	

	private NabPage nabpage;
	
	@Test(groups="UI")
	@Parameters({ "fn","ln","ph","em","url","appURL"})
	public void testNab(String fn,String ln,String ph,String em,String url,String appUrl) {
		
		nabpage = new NabPage(browser,url,fn,ln,ph,em);
		nabpage.url(appUrl)
			   .clcikPersonal()
			   .typeAndsubmit();
		//System.out.println("title is "+nabpage.getTitle());
		Assert.assertEquals(
				nabpage.getTitle(browser), 
				"Consumer Call Centre Request Callback Form - NAB");
		
	}
}
