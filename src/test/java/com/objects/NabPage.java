package com.objects;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.Iterator; 

public class NabPage {
	
	protected WebDriver driver;
	protected String u;
	protected String fn;
	protected String ln;
	protected String ph;
	protected String em;
	
	
	
	//clcikPersonal
	private By personal  = By.xpath("//a[@class='menu-trigger'][contains(text(),'Personal')]");
	private By homeloans = By.xpath("//html/body/div[1]/div/div[3]/div/header/div[3]/nav/ul/li[1]/ul/li[5]/a/span");
	private By enquire =   By.xpath("//p[contains(text(),'Enquire about a new loan')]");
	
	private By shawRoot= By.xpath("//*[@id='contact-form-shadow-root']");
	private By hl =  By.xpath("/html/body/div[1]/div/div[3]/div/header/div[3]/nav/ul/li[1]/ul/li[5]/div/ul/li[1]/a/span");
			
	//typeAndsubmit 
	private By no = By.xpath("//label[2]");
	
	//field-page-Page1-aboutYou-firstName
	private By fname = By.xpath("//input[@id='field-page-Page1-aboutYou-firstName']");					 
	private By lname = By.xpath("//input[@id='field-page-Page1-aboutYou-lastName']");
	private By state = By.xpath("//div[@class='css-1hwfws3 react-select__value-container']");
	private By phone = By.xpath("//input[@id='field-page-Page1-aboutYou-phoneNumber']");
	private By email = By.xpath("//input[@id='field-page-Page1-aboutYou-email']");
	private By submit= By.xpath("//span[contains(text(),'Submit')]");
	

	private By callback = By.xpath("//span[contains(text(),'Request a call back')]");
	
	public NabPage(WebDriver driver,String u,String fn,String ln,String ph,String em) {
		
		System.out.println("Initial nab page object");
		this.u = u;
		this.fn= fn;
		this.ln =ln;
		this.ph =ph;
		this.em =em;
		this.driver=driver;
	}
	
	public NabPage url(String url) {
		driver.get(url);
		wait(3,driver);
		System.out.println("driver wait..");
		return this;
	}
	
	public String getTitle() {
		return driver.getTitle();
	}
	
	
	public NabPage clcikPersonal(){
		
		
		waitElement(10,personal,driver);
		
		Actions actions = new Actions(driver);
		WebElement elementLocator = driver.findElement(personal);
		actions.click(elementLocator).build().perform();
		
		waitElement(10,homeloans,driver);
		elementLocator = driver.findElement(homeloans);
		actions.click(elementLocator).build().perform();
		
		
		try {
			
			driver.findElement(hl).click();
		}catch (Exception error) {
			//driver.get(u);
		}
		
		driver.findElement(callback).click(); // click on Request a call back
		
		//Get shadowroot 
		WebElement shadowHost =driver.findElement(shawRoot);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement shadoweles = (WebElement) js.executeScript("return arguments[0].shadowRoot",shadowHost);
		shadoweles.findElement(By.id("myRadioButton-0")).click();
		
	    //shadowRoot	
	
		//Next button
		Actions actionProvider = new Actions(driver);
		actionProvider.sendKeys(Keys.TAB).build().perform();
		actionProvider.sendKeys(Keys.ENTER).build().perform();
		
		
		String parent=driver.getWindowHandle();
		System.out.println("parent "+ parent);
		
		//child windows
		Set<String> newWindowHands=driver.getWindowHandles();
		Iterator<String> ws= newWindowHands.iterator();
		
		System.out.println("WindowHands size"+newWindowHands);
		
		
		while(ws.hasNext())
		{
			String child_window=ws.next();
			System.out.println("child_window "+child_window);
			
			if(!parent.equals(child_window)) {
				
			System.out.println("switch to child window");
			try {
					Thread.sleep(3000);
				    driver.switchTo().window(child_window);
				}
				catch (Exception error) {
					error.printStackTrace();
				}
			}
	     	
		}
		
		driver.findElement(no).click(); // click yes/no button
		
		
		return this;
	}
	
	
	public void typeAndsubmit ()  {
		
		//type information
		
		driver.findElement(fname).sendKeys(fn);
		driver.findElement(lname).sendKeys(ln);
		driver.findElement(phone).sendKeys(ph);
		driver.findElement(email).sendKeys(em);;
		waitElement(6,state,driver);
		driver.findElement(state).click();
		Actions actionProvider = new Actions(driver);
		actionProvider.sendKeys(Keys.SPACE).build().perform();
		
		//submit it
		driver.findElement(submit).click();
		
	}
	
	public String getTitle(WebDriver driver) {
		
		return driver.getTitle();
	}
		
	public WebElement  waitElement(int secs,By x,WebDriver driver){
		
		
		WebDriverWait wait = new WebDriverWait(driver, secs);
		WebElement element = wait
				.until(ExpectedConditions.elementToBeClickable(x));
		
		return element;
		
	}
	
	public void wait(int secs,WebDriver driver) {
		
		try {
			
			driver.manage().timeouts().implicitlyWait(secs,TimeUnit.SECONDS);
			Thread.sleep(secs*1000);
			
		}catch (Exception error) {
			
			System.out.print("Page is not loaded completely");
		}
		
	}

}
