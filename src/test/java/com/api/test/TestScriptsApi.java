package com.api.test;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import static io.restassured.RestAssured.*;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.sf.json.JSONArray;

import org.testng.Assert;

public class TestScriptsApi {
	
	
	@Test(groups="API")
	@Parameters({ "url_api","lat","lon","Key"})
	public void testCurrentApi(String url_api,String lat,String lon,String Key) {
		
		RestAssured.basePath=url_api+"current?lat="+lat
											 + "&lon="+lon
											 +"&Key="+Key;
		
		Response res = 
				given().log().all()
				      .contentType(ContentType.JSON)
				.when()
				     .get(RestAssured.basePath)
				.then()
				     .statusCode(200)
				     .statusLine("HTTP/1.1 200 OK")
				     .extract().response();
				     //verify status code

		
		String responseBody = res.asString();
		Assert.assertTrue(responseBody.contains("state_code"));
		JsonObject jsonObject = new JsonParser().parse(responseBody).getAsJsonObject();
		Assert.assertTrue(jsonObject.isJsonObject());
		jsonObject = new JsonParser().parse(jsonObject.get("data").toString().replaceAll("[\\[\\]]","")).getAsJsonObject();
		System.out.println(jsonObject.get("state_code").getAsString());
		Assert.assertTrue(jsonObject.get("state_code").getAsString().equals("NY"));

		
	}
	
	@Test(enabled=false,groups="API")
	@Parameters({ "url_api","postcode","Key"})
	public void testForecastApi(String url_api,String postcode,String Key) {
		
		RestAssured.basePath=url_api+"forecast/hourly?postal_code="+postcode
				 												   +"&Key="+Key;
		Response res = 
				given().log().all()
				      .contentType(ContentType.JSON)
				.when()
				     .get(RestAssured.basePath)
				.then()
				     .statusCode(200)
				     .statusLine("HTTP/1.1 200 OK")
				     .extract().response();
		 			 //verify status code
		
		String responseBody = res.asString();
		System.out.println("response body "+ responseBody);
		
		Assert.assertTrue(responseBody.contains("timestamp_utc"));
		Assert.assertTrue(responseBody.contains("weather"));
		
		JsonObject jsonObject = new JsonParser().parse(responseBody.trim()).getAsJsonObject();
		JSONArray jsonArray = JSONArray.fromObject(jsonObject.get("data").toString());
		jsonObject = new JsonParser().parse(jsonArray.get(0).toString()).getAsJsonObject();
		Assert.assertTrue(jsonObject.get("timestamp_utc").getAsString().equals("2020-08-13T04:00:00"));
		System.out.println(jsonObject.get("timestamp_utc"));
		System.out.println(jsonObject.get("weather"));
	}
}
