package log;

import org.testng.TestListenerAdapter;

import com.base.TestBaseSetup;

import org.testng.ITestResult;




public class CustomListener extends TestListenerAdapter {
	
	private int m_count = 0;
	 
	   @Override
	   public void onTestFailure(ITestResult tr) {
	      log(tr.getName()+ "--Test method failed from log\n");
	   }
		 
	   @Override
	   public void onTestSkipped(ITestResult tr) {
	      log(tr.getName()+ "--Test method skipped from log\n");
	   }
		 
	   @Override
	   public void onTestSuccess(ITestResult tr) {
	      log(tr.getName()+ "--Test method success from log\n");
	      TestBaseSetup.takeSnapShot(TestBaseSetup.getDriver(),tr.getName());
	   }
		 
	   private void log(String string) {
	      System.out.print(string);
	      if (++m_count % 40 == 0) {
	         System.out.println("");
	      }
	   }

}
